<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store' , 'update' , 'delete']);
    }

    public function index()
    {
        $post = Post::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Daftar data post berhasil',
            'data' => $post
        ]);
    }

    public function store(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest , [
            'judul' => 'required',
            'deskripsi' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = auth()->user();

        $post = Post::create([
            'judul' => $request->judul,
            'deskripsi' => $request->deskripsi,
            'user_id' => $user->id
        ]);

        if ($post){
            return response()->json([
                'success' => true,
                'message' => 'Data post berhasil dibuat',
                'data' => $post
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data post gagal dibuat',
        ], 409);
    }

    public function show($id)
    {
        $post = Post::find($id);

        if ($post)
        {
            return response()->json([
                'success' => true,
                'message' => 'Data post berhasil ditampilkan',
                'data' => $post
            ], 200);
        }


            return response()->json([
                'success' => false,
                'message' => 'Data dengan id: ' . $id . ' tidak ditemukan',
            ], 404);
    }

    public function update(Request $request , $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest , [
            'judul' => 'required',
            'deskripsi' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $post = Post::find($id);


        if ($post)
        {

            $user = auth()->user();

            if($post->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login',
                    'data' => $post
                ] , 403);

            }

            $post->update([
                'judul' => $request->judul,
                'deskripsi' => $request->deskripsi,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data dengan id: ' . $id . ' berhasil di update',
                'data' => $post
            ]);
        }

        return response()->json([
                'success' => false,
                'message' => 'Data dengan id: ' . $id . ' tidak ditemukan',
        ], 404);
    }

    public function destroy($id)
    {

        $post = Post::find($id);

        if ($post)
        {

            $user = auth()->user();

            if ($post->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login',
                ], 403);
            }

            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data post berhasil dihapus',
                'data' => $post
            ], 200);
        }


            return response()->json([
                'success' => false,
                'message' => 'Data dengan id: ' . $id . ' tidak ditemukan',
            ], 404);
    }

}
