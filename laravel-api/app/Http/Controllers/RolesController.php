<?php

namespace App\Http\Controllers;

use App\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RolesController extends Controller
{
    public function index()
    {
        $roles = Roles::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Daftar data roles berhasil',
            'data' => $roles
        ]);
    }

    public function store(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest , [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $roles = Roles::create([
            'name' => $request->name,
        ]);

        if ($roles){
            return response()->json([
                'success' => true,
                'message' => 'Data roles berhasil dibuat',
                'data' => $roles
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data roles gagal dibuat',
        ], 409);
    }

    public function show($id)
    {
        $roles = Roles::find($id);

        if ($roles)
        {
            return response()->json([
                'success' => true,
                'message' => 'Data roles berhasil ditampilkan',
                'data' => $roles
            ], 200);
        }


            return response()->json([
                'success' => false,
                'message' => 'Data dengan id: ' . $id . ' tidak ditemukan',
            ], 404);
    }

    public function update(Request $request , $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest , [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $roles = Roles::find($id);

        if ($roles)
        {
            $roles->update([
                'name' => $request->name,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data dengan id: ' . $id . ' berhasil di update',
                'data' => $roles
            ]);
        }

        return response()->json([
                'success' => false,
                'message' => 'Data dengan id: ' . $id . ' tidak ditemukan',
        ], 404);
    }

    public function destroy($id)
    {

        $roles = Roles::find($id);

        if ($roles)
        {
            $roles->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data roles berhasil dihapus',
                'data' => $roles
            ], 200);
        }


            return response()->json([
                'success' => false,
                'message' => 'Data dengan id: ' . $id . ' tidak ditemukan',
            ], 404);
    }

}
