<?php

namespace App\Http\Controllers\Auth;

use App\Users;
use App\OtpCode;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest , [
            'name' => 'required',
            'email' => 'required|unique:users,email|email',
            'username' => 'required|unique:users,username'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $users = Users::create($allRequest);


        do {
            $random = mt_rand( 100000 , 999999 );
            $check = OtpCode::where('otp' , $random)->first();

        } while ($check);

        $now = Carbon::now();

        $otp_codes = OtpCode::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(5),
            'user_id' => $users->id
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Data User Berhasil Dibuat',
            'data' => [
                'users' => $users,
                'otp_codes' => $otp_codes
            ]
            ]);
    }
}
