<?php

namespace App\Http\Controllers\Auth;

use App\Users;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest , [
            'otp' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $otp_codes = OtpCode::where('otp' , $request->otp)->first();

        if(!$otp_codes)
        {
            return response()->json([
                'success' => false,
                'message' => 'OTP Code tidak ditemukan'
            ], 400);
        }

        $now = Carbon::now();

        if($now > $otp_codes->valid_until)
        {
            return response()->json([
                'success' => false,
                'message' => 'OTP Code tidak berlaku lagi'
            ], 400);
        }

        $users = Users::find($otp_codes->user_id);
        $users->update([
            'email_verified_at' => $now
        ]);

        $otp_codes->delete();

        return response()->json([
            'success' => true,
            'message' => 'User berhasil di verifikasi',
            'data' => $users
        ], 200);
    }
}
