<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Mail\PostAuthorMail;
use Illuminate\Http\Request;
use App\Mail\CommentsAuthorMail;
use App\Events\CommentStoredEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CommentsController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth:api')->except(['index', 'show']);
    }

    public function index()
    {
        $comment = Comments::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Daftar data comments berhasil',
            'data' => $comment
        ]);
    }

    public function store(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest , [
            'content' => 'required',
            'post_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $comment = Comments::create([
            'content' => $request->content,
            'post_id' => $request->post_id,
        ]);

        //Memanggil CommentStoredEvent
        event(new CommentStoredEvent($comment));

        //Ini dikirim kepada yang memiliki Post
        // Mail::to($comment->post->user->email)->send(new PostAuthorMail($comment));

        //Ini dikirim kepada yang memiliki Comments
        // Mail::to($comment->user->email)->send(new CommentsAuthorMail($comment));

        if ($comment){
            return response()->json([
                'success' => true,
                'message' => 'Data comments berhasil dibuat',
                'data' => $comment
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data comments gagal dibuat',
        ], 409);
    }

    public function show($id)
    {
        $comment = Comments::find($id);

        if ($comment)
        {
            return response()->json([
                'success' => true,
                'message' => 'Data comments berhasil ditampilkan',
                'data' => $comment
            ], 200);
        }


            return response()->json([
                'success' => false,
                'message' => 'Data dengan id: ' . $id . ' tidak ditemukan',
            ], 404);
    }

    public function update(Request $request , $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest , [
            'content' => 'required',
            'post_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $comment = Comments::find($id);

        if ($comment)
        {

            $user = auth()->user();

            if ($comment->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data comments bukan milik user login',
                ], 403);
            }


            $comment->update([
                'content' => $request->content,
                'post_id' => $request->post_id,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data dengan id: ' . $id . ' berhasil di update',
                'data' => $comment
            ]);
        }

        return response()->json([
                'success' => false,
                'message' => 'Data dengan id: ' . $id . ' tidak ditemukan',
        ], 404);
    }

    public function destroy($id)
    {

        $comments = Comments::find($id);

        if ($comment)
        {

            $user = auth()->user();

            if ($comment->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data comments bukan milik user login',
                ], 403);
            }

            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data comments berhasil dihapus',
                'data' => $comment
            ], 200);
        }


            return response()->json([
                'success' => false,
                'message' => 'Data dengan id: ' . $id . ' tidak ditemukan',
            ], 404);
    }

}
