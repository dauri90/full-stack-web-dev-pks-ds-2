<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// //API Post
// Route::get('/post' , 'PostController@index');
// Route::post('/post' , 'PostController@store');
// Route::get('/post/{id}' , 'PostController@show');
// Route::put('/post/{id}' , 'PostController@update');
// Route::delete('/post/{id}' , 'PostController@destroy');

// //API Roles
// Route::get('/roles' , 'RolesController@index');
// Route::post('/roles' , 'RolesController@store');
// Route::get('/roles/{id}' , 'RolesController@show');
// Route::put('/roles/{id}' , 'RolesController@update');
// Route::delete('/roles/{id}' , 'RolesController@destroy');

// //API Comments
// Route::get('/comments' , 'CommentsController@index');
// Route::post('/comments' , 'CommentsController@store');
// Route::get('/comments/{id}' , 'CommentsController@show');
// Route::put('/comments/{id}' , 'CommentsController@update');
// Route::delete('/comments/{id}' , 'CommentsController@destroy');

Route::apiResource('post' , 'PostController');
Route::apiResource('comments' , 'CommentsController');
Route::apiResource('roles' , 'RolesController');


Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth'
] , function(){

    Route::post('register' , 'RegisterController')->name('auth.register');
    Route::post('regenerate-otp-codes' , 'RegenerateOtpCodeController')->name('auth.regenerate-otp-codes');
    Route::post('verification' , 'VerificationController')->name('auth.verification');
    Route::post('update-password' , 'UpdatePasswordController')->name('auth.update_password');
    Route::post('login' , 'LoginController')->name('auth.login');

});

